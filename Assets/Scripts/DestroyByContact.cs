﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;

	void Start()
	{
		GameObject gameCotrollerObject = GameObject.FindWithTag ("GameController");
		if (gameCotrollerObject != null)
		{
			gameController = gameCotrollerObject.GetComponent<GameController> ();
		}
		if(gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnCollisionEnter(Collision other)
	{
		//Debug.Log(other.gameObject.tag );
		if (other.gameObject.tag == "Boundary") 
		{
			return;
		}

		Instantiate(explosion, transform.position, transform.rotation);

		if (other.gameObject.tag == "Player") 
		{
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver ();
		}

		gameController.AddScore (scoreValue);
		Destroy (other.gameObject);
		Destroy (gameObject);
	}
}

